const express = require('express');
const app = express();
app.use(express.json())
const port = 4000;

let courses = [
  

  {
    name: "Python 101",
    description: "Learn Python",
    price: 25000
  },
  {
    name: "ReactJS 101",
    description: "Learn React",
    price: 35000
  },
  {
    name: "ExpressJS 101",
    description: "Learn ExpressJS",
    price: 28000
  },


];

let users = [
  
  {
    email: "marybell_knight",
    password: "merrymarybell"
  },
  {
    email: "janedoePriest",
    password: "jobPriest100"
  },
  {
    email: "kimTofu",
    password: "dubuTofu"
  }


]


 app.get('/', (req, res) => {
   
   res.send("Hello from our first ExpressJS route!");
 
 })

 app.post('/',(req,res) =>{

  res.send("Hello from our first ExpressJS Post Method Route!")

 })

 app.put('/',(req,res) =>{

  res.send("Hello from our first ExpressJS Put Method Route!")

 })

 app.delete('/',(req,res) =>{

  res.send("Hello from our first ExpressJS delete Method Route!")

 })

 app.get('/courses',(req,res)=>{

  res.send(courses);

 })

 app.post('/courses',(req,res) =>{

  //console.log(req.body)

  courses.push(req.body);
  //console.log(courses);

  res.send(courses);

 })

 app.get('/users',(req,res)=>{

  res.send(users);

 })

 app.post('/users',(req,res) =>{

  //console.log(req.body)
  users.push(req.body);
  //console.log(users);
  res.send(users);

 })

 app.delete('/users',(req,res)=>{

  users.pop(users);
  res.send("user has been deleted successfully");

 })

app.listen(port, () => 
  console.log(`Example app listening on port ${port}`)
) 